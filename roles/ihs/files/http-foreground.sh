#!/bin/sh
set -e

# Apache gets grumpy about PID files pre-existing
rm -f /opt/IBM/HTTPServer/logs/httpd.pid

HTTPD='/opt/IBM/HTTPServer/bin/httpd -d /opt/IBM/HTTPServer'

if test -f /opt/IBM/HTTPServer/bin/envvars; then
  . /opt/IBM/HTTPServer/bin/envvars
fi

ULIMIT_MAX_FILES="ulimit -S -n `ulimit -H -n`"
if [ "x$ULIMIT_MAX_FILES" != "x" ] ; then
    $ULIMIT_MAX_FILES
fi

# start HTTPd server
$HTTPD -k start -e info

# stop HTTPd on 'docker stop'
trap '$HTTPD -k stop' SIGTERM

# wait for signal from Docker
while true; do :; done