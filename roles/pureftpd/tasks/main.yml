---
# This task list sets up FTP configurations and software

- name: create group
  group:
    name: "{{ ftp_service_user.group }}"
    gid: "{{ ftp_service_user.gid }}"

- name: create user
  user:
    name: "{{ ftp_service_user.username }}"
    group: "{{ ftp_service_user.group }}"
    uid: "{{ ftp_service_user.uid }}"
    shell: /usr/sbin/nologin
    createhome: no
    home: /srv/ftp

- name: create WCaaS directories
  file:
    path: "{{ item }}"
    state: directory
    mode: "0755"
  with_items:
    - /opt/wcaas
    - /opt/wcaas/bin
    - /opt/wcaas/etc
    - /opt/wcaas/etc/services
    - /opt/wcaas/etc/services/common
    - /opt/wcaas/tmp
    - /opt/wcaas/var
    - /opt/wcaas/www
    - /etc/ssl/private

- name: install from source
  import_tasks: tasks/install.yml

- name: prepare pure-ftpd directories
  file:
    path: "{{ item }}"
    state: directory
  with_items:
    - /etc/pure-ftpd
    - /etc/pure-ftpd/auth
    - /etc/pure-ftpd/conf
    - /var/log/pure-ftpd

- name: create ftp home directory
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ ftp_service_user.username }}"
    group: "{{ ftp_service_user.group }}"
  with_items:
    - /srv/ftp

- name: pure-ftpd config files
  copy:
    src: "{{ config_templates[ item ].src }}"
    dest: "{{ config_templates[ item ].dest }}"
    owner: root
    group: root
    mode: "{{ config_templates[ item ].mode }}"
  with_items: "{{ config_templates }}"

- name: pure-ftpd config options
  copy:
    src: "{{ item }}"
    dest: "/etc/pure-ftpd/conf/{{ item }}"
    owner: root
    group: root
    mode: "0644"
  with_items: "{{ config_options }}"

- name: pure-ftpd passive config options
  template:
    src: ForcePassiveIP.j2
    dest: "/etc/pure-ftpd/conf/ForcePassiveIP"
    owner: root
    group: root
    mode: "0644"

- name: pure-ftpd auth options
  file:
    src: "/etc/pure-ftpd/conf/{{ item }}"
    dest: "/etc/pure-ftpd/auth/{{ item }}"
    state: link
  with_items:
    - ExtAuth

- name: generate dh certificate
  command: "openssl dhparam -out /etc/ssl/private/pure-ftpd-dhparams.pem 512"
  args:
    chdir: "/etc/ssl/private/"
    creates: "/etc/ssl/private/pure-ftpd-dhparams.pem"

- name: prepare PEM file for pure-ftpd
  shell: "openssl req -x509 -nodes -newkey rsa:2048 -sha256 -days 3650 -keyout /etc/ssl/private/pure-ftpd.pem -out /etc/ssl/private/pure-ftpd.pem -subj '{{certificate.subject}}' && chmod 600 /etc/ssl/private/*.pem"
  args:
    chdir: "/etc/ssl/private/"
